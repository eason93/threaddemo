package com.example.threadDemo;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class ThreadPoolExecutorDemo {
    public void threadDemo() {
        List<Future> futureList = new ArrayList<>();
//        ThreadPoolExecutor executorService = new ThreadPoolExecutor(3, 3,
//                1L, TimeUnit.SECONDS,
//                new ArrayBlockingQueue<Runnable>(5),  //固定长度
//                new ThreadPoolExecutor.AbortPolicy());    //默认抛弃方法
        ThreadPoolExecutor executorService = new ThreadPoolExecutor(3, 3,
                0L, TimeUnit.SECONDS,
                new SynchronousQueue<Runnable>(),  //固定长度
                new ThreadPoolExecutor.AbortPolicy());    //默认抛弃方法
        System.out.println("begin:" + System.currentTimeMillis());
        try {
            for (int i = 0; i < 10; i++) {
                final String threadNum = i + "";
                Future future = executorService.submit(() -> {
                    System.out.println(System.currentTimeMillis() / 1000);
                    Thread.sleep(200);
                    return "threadDemo" + threadNum;
                });
                futureList.add(future);
            }
        } catch (Exception e) {

        }
        System.out.println("end:" + System.currentTimeMillis());
        executorService.shutdown();
        futureList.forEach((fu) -> {
            try {
                System.out.println("Error" + fu.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        });
//        executorService.shutdown();
    }

    public void threadDemo2() {
        ThreadPoolExecutor executorService = new ThreadPoolExecutor(3, 3,
                0L, TimeUnit.SECONDS,
                new ArrayBlockingQueue<Runnable>(5),  //固定长度
                new ThreadPoolExecutor.AbortPolicy());    //默认抛弃方法

        System.out.println("begin:" + System.currentTimeMillis());
        try {
            for (int i = 0; i < 10; i++) {
                final String threadNum = i + "";
                executorService.execute(() -> {
                    System.out.println("threaddDemo2:" + System.currentTimeMillis() / 1000);
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                });
            }
        } catch (Exception e) {

        }
        System.out.println("end:" + System.currentTimeMillis());
        executorService.shutdown();
//        executorService.shutdown();
    }

    /**
    这是一个错误的示例
     原意是想创建一个corePoolSize为3的线程池，然后通过开启10个进程达到固定长度的Queue的长度，然后报错

     报错是达到了，然而这个错误是抛错给主线程的，导致主线程运行不下去（因为你没有处理这个错误），正确的方法是你还需要handle这个错误
     */
    public void threadDemo3() {
        List<Future> futureList = new ArrayList<>();
        ThreadPoolExecutor executorService = new ThreadPoolExecutor(3, 3,
                1L, TimeUnit.SECONDS,
                new ArrayBlockingQueue<Runnable>(5),  //固定长度
                new ThreadPoolExecutor.AbortPolicy());    //默认抛弃方法
        System.out.println("begin:" + System.currentTimeMillis());
        for (int i = 0; i < 10; i++) {
            final String threadNum = i + "";
            Future future = executorService.submit(() -> {
                System.out.println(System.currentTimeMillis() / 1000);
                Thread.sleep(200);
                return "threadDemo" + threadNum;
            });
            futureList.add(future);
        }
        System.out.println("end:" + System.currentTimeMillis());
        executorService.shutdown();
        futureList.forEach((fu) -> {
            try {
                System.out.println("Error" + fu.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        });
//        executorService.shutdown();
    }

    public static void main(String[] args) {
        new ThreadPoolExecutorDemo().threadDemo();
        new ThreadPoolExecutorDemo().threadDemo2();
    }
}
