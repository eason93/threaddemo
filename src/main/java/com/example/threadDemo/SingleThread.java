package com.example.threadDemo;

import java.util.concurrent.*;

public class SingleThread {
    public static void main(String[] args){
        /**
         * 三种方式差不多
         */
        ExecutorService executors=Executors.newFixedThreadPool(5);
//        ExecutorService executors1=Executors.newCachedThreadPool();
//        ExecutorService executors2=Executors.newSingleThreadExecutor();
        Future<Object> future =executors.submit(new Callable<Object>() {
            public Object call() throws Exception {
                //测试代码，看看get的结果
                if(true){
                    System.out.println("test");
                    return "123";
                }
                throw new RuntimeException("exception in call~");// 该异常会在调用Future.get()时传递给调用者
            }
        });
        try {
            System.out.println(future.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        System.out.println("Thread end;");
        //不shutdown，进程不停止；需要再看看
//        executors.shutdown();
    }
}
